package pkg1;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;

import javax.swing.*;

import java.net.URLClassLoader;

public class MainMenu extends JPanel {

	 JRadioButton jrbPractice = new JRadioButton("study");
	 JRadioButton jrbTest = new JRadioButton("test");
	 JRadioButton jrbScores = new JRadioButton("view top scores");
	 JRadioButton jrbEdit = new JRadioButton("edit");
	 ButtonGroup buttonGroup;
	 String[] regions = (new Regions()).getRegions();
	 JPanel jpSelectRegion = new JPanel();
	 JComboBox jcbSelectRegion = new JComboBox();
	 JLabel jlSelectRegion = new JLabel("Select a region: ");
	 JPanel jpPlayerName = new JPanel();
	 JLabel jlPlayerName = new JLabel("Enter player name: ");
	 JTextField jtfPlayerName = new JTextField(8);
	 String[] difficulty = { "Easy", "Hard" };
	 JPanel jpDifficulty = new JPanel();
	 JLabel jlDifficulty = new JLabel();
	 JComboBox jcbDifficulty = new JComboBox();
	 JButton jbOk = new JButton("Ok");


	public MainMenu() {
		setLayout(new GridLayout(5, 1));
		jrbPractice.setHorizontalTextPosition(SwingConstants.LEFT);
		jrbTest.setHorizontalTextPosition(SwingConstants.LEFT);
		jrbScores.setHorizontalTextPosition(SwingConstants.LEFT);
		jrbEdit.setHorizontalTextPosition(SwingConstants.LEFT);
		buttonGroup = new ButtonGroup();
		
		buttonGroup.add(jrbPractice);
		buttonGroup.add(jrbTest);
		buttonGroup.add(jrbScores);
		buttonGroup.add(jrbEdit);
		
		
		//sets jrbPractice as the default selected radio button and sets up jcbSelectRegion
		jrbPractice.setSelected(true);
		for (String s : regions) {
			jcbSelectRegion.addItem(s);
		}
		
		JPanel menuOptions = new JPanel();
		menuOptions.add(jrbPractice);
		menuOptions.add(jrbTest);
		menuOptions.add(jrbScores);
		menuOptions.add(jrbEdit);
		add(menuOptions);

		jpSelectRegion.add(jlSelectRegion);
		jpSelectRegion.add(jcbSelectRegion);
		add(jpSelectRegion);

		jpPlayerName.add(jlPlayerName);
		jpPlayerName.add(jtfPlayerName);
		jpPlayerName.setVisible(false);
		add(jpPlayerName);

		jpDifficulty.add(jlDifficulty);
		jpDifficulty.add(jcbDifficulty);
		jpDifficulty.setVisible(false);
		add(jpDifficulty);
		
		JPanel jpOk = new JPanel();
		jbOk.setPreferredSize(new Dimension(80, 30));
		jpOk.add(jbOk);
		add(jpOk);
		
		jrbPractice.addActionListener(new MenuOptionListener());
		jrbTest.addActionListener(new MenuOptionListener());
		jrbScores.addActionListener(new MenuOptionListener());
		jrbEdit.addActionListener(new MenuOptionListener());

	}

	public class MenuOptionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == jrbPractice || e.getSource() == jrbEdit|| e.getSource() == jrbTest) {
				jcbSelectRegion.removeAllItems();
				for (String s : regions) {
					jcbSelectRegion.addItem(s);
				}
				jpSelectRegion.setVisible(true);
				if (e.getSource() == jrbTest) {
					jpPlayerName.setVisible(true);
					jcbDifficulty.removeAllItems();
					for (String s : difficulty) {
						jcbDifficulty.addItem(s);
					}
					jlDifficulty.setText("Select Difficulty:");
					jpDifficulty.setVisible(true);

				} else {
					jpPlayerName.setVisible(false);
					jpDifficulty.setVisible(false);
				}
				jbOk.setVisible(true);

			}

			else if (e.getSource() == jrbScores) {
				jpSelectRegion.setVisible(false);
				jpPlayerName.setVisible(false);

				jlDifficulty.setText("View scores by Difficulty:");
				jcbDifficulty.removeAllItems();
				for (String s : difficulty) {
					jcbDifficulty.addItem(s);
				}
				jpDifficulty.setVisible(true);
				jbOk.setVisible(true);
			}

	
		}
	
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.add(new MainMenu());
		frame.setTitle("Main Menu");
		frame.setSize(300, 300);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}