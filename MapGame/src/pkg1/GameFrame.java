package pkg1;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import java.util.*;

import javax.swing.*;
import javax.swing.Timer;
import javax.swing.border.*;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
public class GameFrame extends JFrame{
	

	JCheckBox jcbMute = new JCheckBox("Mute");

	private JLabel jlInstruction;
	Timer timer;
	private JLabel jlMapTitle;
	private	 int score = 0;
	private   int questionCount = 0;
	private JLabel jlScore;
	private boolean isAlreadyAnsweredIncorrectly; //This is false because the subregion hasn't been moved to an incorrect location yet
	private int minutes = 0;
	private int seconds = 0;
	private JLabel jlTime;
	private Region [] subregions;
	private Region [] incorrectSubregions;
	private boolean [] subregionsCheck;//will be used to check if a state has already been selected.
	private Region subregion;
	private Region incorrectSubregion;
	private Region selectedRegion;
	private int tempIndex = 0;
	private String[] goodSoundsPath;
	private String[] badSoundsPath;
	//jlpMain.setLayout(null); by default JLayeredPane does not have a layout manager (i.e., null).
	private JLayeredPane jlpMain = new JLayeredPane();
	private JPanel jpAllButSubregion = new JPanel();
	private JPanel jpDrag = new JPanel();
	DragListener dragListener = new DragListener();
	ReleaseListener releaseListener = new ReleaseListener();
	ArrayList<String> missedSubregions = new ArrayList<>();
	ArrayList<String> correctSubregions = new ArrayList<>();
	String playerName = "";
	JRadioButton jrbCurrentlySelectedMenuOption;
	 JFrame scoreBoardFrame;
	JLabel mapToBeDisplayed = new JLabel();
	MainMenu mainMenu ;
	int editModeIndex;
	
	EditDrag editDrag = new EditDrag();
	MouseDoubleClickedListener doubleClickedListener = new MouseDoubleClickedListener();
	private GameFrame(){
		jpAllButSubregion.setLayout(new BorderLayout());
		
		//includes game control, and score information
		JPanel jpLeftUpper = new JPanel();
		jpLeftUpper.setLayout(new BorderLayout());
	
		JPanel jpMute = new JPanel();//panel for mute combobox
		jpMute.setLayout(new BorderLayout());
		jpMute.setBorder(new TitledBorder("Game Controls"));
		
		jpMute.add(jcbMute, BorderLayout.WEST);
		jpLeftUpper.add(jpMute, BorderLayout.NORTH);
		
		
		JPanel jpScoreInformation = new JPanel();
		jpScoreInformation.setLayout(new BorderLayout());
		jpScoreInformation.setBorder(new TitledBorder("Score Information"));
	    jlScore = new JLabel("Score: 0/0 of 0 questions");
	    jlTime = new JLabel("Time: 0 minutes 0 seconds");
		jpScoreInformation.add(jlScore, BorderLayout.NORTH);
		jpScoreInformation.add(jlTime, BorderLayout.SOUTH);

	    jpLeftUpper.add(jpScoreInformation, BorderLayout.SOUTH);
	    
		//drag panel
		
		jpDrag.setBorder(new TitledBorder(""));
		jpDrag.setPreferredSize(new Dimension(325, 300));
		
		JPanel jpWest = new JPanel();
		jpWest.setLayout(new BorderLayout());
		jpWest.add(jpLeftUpper, BorderLayout.NORTH);
		
		  mainMenu = new MainMenu();
		jrbCurrentlySelectedMenuOption =  mainMenu.jrbPractice; //it's jrbPractice by default

		mainMenu.jbOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent c) {
				
			if(mainMenu.jrbPractice.isSelected()){
				if(jrbCurrentlySelectedMenuOption == mainMenu.jrbTest && timer.isRunning())
				{
					timer.stop();
					int optionSelected = JOptionPane.showConfirmDialog(null,"<html>Selecting the " + mainMenu.jrbPractice.getText() +" option will end the game.<br>Are you sure you want to end the game?</html>", "Quiet  Game?" ,JOptionPane.YES_NO_OPTION);
					if(optionSelected == JOptionPane.NO_OPTION || optionSelected == JOptionPane.CLOSED_OPTION){
						timer.start();
						return;
					}
				
				}
				jrbCurrentlySelectedMenuOption = mainMenu.jrbPractice;
				//setting up the map for practice
				String regionName = (String)mainMenu.jcbSelectRegion.getSelectedItem();

				Class metaObject = this.getClass();
				System.out.println();
				String urlPractice = metaObject.getResource(regionName + "/" + regionName + " Practice.png").toString().replace("file:/", "").replaceAll("%20", " ");
				
				mapToBeDisplayed.setIcon(new ImageIcon(urlPractice));
				jpAllButSubregion.add(mapToBeDisplayed, BorderLayout.EAST);
				jpAllButSubregion.setBounds(0,0, jpAllButSubregion.getPreferredSize().width, jpAllButSubregion.getPreferredSize().height);
				jlpMain.removeAll();
				jlpMain.add(jpAllButSubregion);
				jpDrag.setBorder(new TitledBorder(""));
		
				reset();
			}
			
			else if(mainMenu.jrbTest.isSelected()){
				if(jrbCurrentlySelectedMenuOption == mainMenu.jrbTest && timer.isRunning()){
					
					timer.stop();
					int optionSelected = JOptionPane.showConfirmDialog(null,"<html>Selecting the " + mainMenu.jrbTest.getText() +" option will end the game.<br>Are you sure you want to end the game?</html>", "Quiet  Game?" ,JOptionPane.YES_NO_OPTION);
					if(optionSelected == JOptionPane.NO_OPTION || optionSelected == JOptionPane.CLOSED_OPTION){

						timer.start();
						return;
					}
				
				}
				
				if(mainMenu.jtfPlayerName.getText().equals("")){
					JOptionPane.showMessageDialog(null, "Please Enter a name.");
					return;
				}
				reset();
				jrbCurrentlySelectedMenuOption = mainMenu.jrbTest;
				
			
				//setting up the map for test
		selectedRegion = new Region((String)mainMenu.jcbSelectRegion.getSelectedItem(),  (String)mainMenu.jcbDifficulty.getSelectedItem());
	
				 playerName = (String)mainMenu.jtfPlayerName.getText().replaceAll(" ", "_");
			
				mapToBeDisplayed.setIcon(selectedRegion.getIcon());
				jpAllButSubregion.add(mapToBeDisplayed, BorderLayout.EAST);
				jpAllButSubregion.setBounds(0,0, jpAllButSubregion.getPreferredSize().width, jpAllButSubregion.getPreferredSize().height);
				jlpMain.removeAll();
				jlpMain.add(jpAllButSubregion);
				
				//setting up the subregions 
				subregions = selectedRegion.getSubregions();
				incorrectSubregions = selectedRegion.getIncorrectSubregions();
				subregionsCheck = new boolean[subregions.length];
				for(boolean e: subregionsCheck){
					e = false;
				}
				
				//setting up score information
				jlScore.setText("Score: 0/0 of " +selectedRegion.subregions.length + " questions");
			    jlTime.setText("Time: 0 minutes 0 seconds");
			    
			    //setting up drag panel
			    jpDrag.setBorder(new TitledBorder(selectedRegion.subregionType+" to drag onto map"));
			    startGame();
				
			
			}
			else if (mainMenu.jrbScores.isSelected()) {
					if (scoreBoardFrame == null) {
						scoreBoardFrame = new JFrame("Score Board");
					}
					else{
						scoreBoardFrame.getContentPane().removeAll();
						
					}
					try {
						scoreBoardFrame.add(ScoreBoard.getInstance((String)mainMenu.jcbDifficulty.getSelectedItem()));
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					scoreBoardFrame.pack();
					scoreBoardFrame.setLocationRelativeTo(null);
					scoreBoardFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
					scoreBoardFrame.setVisible(true);

				}
			else if(mainMenu.jrbEdit.isSelected()){
				if(jrbCurrentlySelectedMenuOption == mainMenu.jrbTest && timer.isRunning())
				{
					timer.stop();
					int optionSelected = JOptionPane.showConfirmDialog(null,"<html>Selecting the " + mainMenu.jrbPractice.getText() +" option will end the game.<br>Are you sure you want to end the game?</html>", "Quiet  Game?" ,JOptionPane.YES_NO_OPTION);
					if(optionSelected == JOptionPane.NO_OPTION || optionSelected == JOptionPane.CLOSED_OPTION){
						timer.start();
						return;
					}
				
				}
				reset();
				jrbCurrentlySelectedMenuOption = mainMenu.jrbEdit;
				//setting up the map for test
		selectedRegion = new Region((String)mainMenu.jcbSelectRegion.getSelectedItem(),  "Easy");
		mapToBeDisplayed.setIcon(selectedRegion.getIcon());
		jpAllButSubregion.add(mapToBeDisplayed, BorderLayout.EAST);
		jpAllButSubregion.setBounds(0,0, jpAllButSubregion.getPreferredSize().width, jpAllButSubregion.getPreferredSize().height);
		jlpMain.removeAll();
		jlpMain.add(jpAllButSubregion);
		subregions = selectedRegion.getSubregions();
		 //setting up drag panel
	    jpDrag.setBorder(new TitledBorder(selectedRegion.subregionType+" to drag onto map"));
	    setSubregionEditMode();
			}
			}
		});
		
		
		jpWest.add(mainMenu);
		jpWest.add(jpDrag, BorderLayout.SOUTH);
		
		jpAllButSubregion.add(jpWest, BorderLayout.WEST);
		jpAllButSubregion.setBounds(0,0, jpAllButSubregion.getPreferredSize().width, jpAllButSubregion.getPreferredSize().height);
	
		jlpMain.add(jpAllButSubregion);
		add(jlpMain);
		
	
		
		Class metaObject = this.getClass();
		String urlBad = metaObject.getResource("Audio/Bad Feedback").toString().replaceAll("%20", " ");
		String urlGood = metaObject.getResource("Audio/Good Feedback").toString().replaceAll("%20", " ");;
		badSoundsPath =(new File(urlBad.replace("file:/", ""))).list();
		goodSoundsPath = (new File(urlGood.replace("file:/", ""))).list();
		for(int i = 0; i < badSoundsPath.length; i++){
			badSoundsPath[i] =  urlBad.toString().replace("file:/", "")+"/" + badSoundsPath[i];
		}
		for(int i = 0; i < goodSoundsPath.length; i++){
			goodSoundsPath[i] =  urlGood.toString().replace("file:/", "") +"/" + goodSoundsPath[i];
		}
		
	}
	public void setSubregionEditMode(){
		if(editModeIndex < subregions.length){
			subregion = subregions[editModeIndex++];
			int subregionWidth = subregion.getPreferredSize().width;
			int subregionHeight = subregion.getPreferredSize().height;
			subregion.setBounds(jpDrag.getX() + (jpDrag.getWidth()/2) - (subregionWidth/2), jpDrag.getY()+ (jpDrag.getWidth()/2)-(subregionHeight/2),	subregionWidth, subregionHeight);
			jlpMain.add(subregion);
			jlpMain.setLayer(subregion, 2);
			subregion.addMouseMotionListener(editDrag);
			subregion.addMouseListener(doubleClickedListener);
			
		}
	
	}
	class MouseDoubleClickedListener extends MouseAdapter{

		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub
			System.out.println(e.getClickCount());
			if(e.getClickCount() == 2){
				Point currentLocation = new Point((subregion.getX()-mapToBeDisplayed.getX()), + (subregion.getY()-mapToBeDisplayed.getY()));
				
				PrintWriter pw;
				try {
					pw = new PrintWriter(new FileOutputStream(selectedRegion.pointsFile, true));
					pw.println(currentLocation.x + " , " + currentLocation.y);
					pw.close();
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				subregion.removeMouseMotionListener(dragListener);
				subregion.removeMouseListener(doubleClickedListener);
				jlpMain.setLayer(subregion, 1);
				subregion = null;
			setSubregionEditMode();
		}

}		
	}
	class EditDrag extends MouseMotionAdapter{
		
		@Override
		public void mouseDragged(MouseEvent e) {
			if(subregion != null && jlpMain.getMousePosition() != null){
			subregion.setLocation(jlpMain.getMousePosition().x - subregion.getWidth()/2, jlpMain.getMousePosition().y - subregion.getHeight()/2);
			Point currentLocation = new Point((subregion.getX()-mapToBeDisplayed.getX()), + (subregion.getY()-mapToBeDisplayed.getY()));
			
			subregion.setToolTipText((currentLocation.x) + " ,  " + (currentLocation.y));
		}
		}
	
		
	}
	public void setRandomSubregion(){
		ArrayList<Integer> nonSelectedIndices = new ArrayList<>();
		for(int i = 0; i < subregionsCheck.length; i++){
			if(subregionsCheck[i] == false){
				nonSelectedIndices.add(i);
			}
		}
		if(nonSelectedIndices.size() > 0){
		int randomIndexInNonSelectedIndices = (int)(Math.random()*nonSelectedIndices.size());//a random index of nonSelectedIndices
		
		subregion = subregions[nonSelectedIndices.get(randomIndexInNonSelectedIndices)];
		subregionsCheck[nonSelectedIndices.get(randomIndexInNonSelectedIndices)] = true;
		incorrectSubregion = incorrectSubregions[nonSelectedIndices.get(randomIndexInNonSelectedIndices)];
		}
		
		/*This is for the very last subregion that gets put in the correct location
		After the last subregion has been inserted by the program, this method gets called to disable moving
		subregion and also calls the method that ends the game.
		*/
		else{
			subregion = null;
			timer.stop();
			if(!jcbMute.isSelected()){
			playSound(new String[]{this.getClass().getResource("Audio/End Game Sound.wav").toString().replace("file:/", "").replaceAll("%20", " ")});
			}
			JFrame frame = new Results(playerName, selectedRegion, score, minutes, seconds, correctSubregions.toArray(new String[correctSubregions.size()]), missedSubregions.toArray(new String[missedSubregions.size()]));
			frame.pack();
			frame.setLocationRelativeTo(null);
			frame.pack();
			frame.setVisible(true);
			frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		}
	}
	public void startGame(){
		
		//initalizes subregion
		setRandomSubregion();
		subregion.addMouseMotionListener(dragListener);
		subregion.addMouseListener(releaseListener);
			int subregionWidth = subregion.getPreferredSize().width;
			int subregionHeight = subregion.getPreferredSize().height;
			subregion.setBounds(jpDrag.getX() + (jpDrag.getWidth()/2) - (subregionWidth/2), jpDrag.getY()+ (jpDrag.getWidth()/2)-(subregionHeight/2),	subregionWidth, subregionHeight);
			jlpMain.add(subregion);
			jlpMain.setLayer(subregion, 2);
			 timer = new Timer(1000, new TimerActionListener());
			 timer.start();
	}
	public class TimerActionListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
		long currentTime = System.currentTimeMillis();
		if(seconds == 59){
			seconds = 0;
			minutes ++;
		}
		else{
			seconds++;
		}
		jlTime.setText("Time: "+minutes+ " "+((minutes == 1)? "minute ":"minutes ") + seconds + " "+((seconds == 1)?"second":"seconds"));
		}
		
	}
public class ReleaseListener extends MouseAdapter{
	@Override
	public void mouseReleased(MouseEvent arg0) {
	Point currentLocation = new Point((subregion.getX()-mapToBeDisplayed.getX()), + (subregion.getY()-mapToBeDisplayed.getY()));
	

		int correctX = subregion.correctLocation.x;
		int correctY = subregion.correctLocation.y;
		int currentX = currentLocation.x;
		int currentY = currentLocation.y;
		
		//if The subregion is place in the incorrect location ...
	if(!(Math.abs(correctX-currentX) <= 20 && Math.abs(correctY-currentY) <= 20)){
		
		//If the subregion hasn't been answered incorrectly already then ...
		if(!isAlreadyAnsweredIncorrectly){
			updateGame(false);
			if(!jcbMute.isSelected()){
			playSound(badSoundsPath);
			}
			isAlreadyAnsweredIncorrectly = true;
			missedSubregions.add(subregion.regionName);
		}
		else{
			if(!jcbMute.isSelected()){
				playSound(new String[]{this.getClass().getResource("Audio/Bad Feedback/incorrect.wav").toString().replace("file:/", "").replaceAll("%20", " ")});
				updateGame(false);
				}
		}
		
	}
	else{
		if(!jcbMute.isSelected()){
		playSound(goodSoundsPath, .70);
		}
		
			updateGame(true);
			
	
		
	}
	
	
	}
}

public class DragListener extends MouseMotionAdapter{
	@Override
	public void mouseDragged(MouseEvent e) {
		if(subregion != null && jlpMain.getMousePosition() != null){
		subregion.setLocation(jlpMain.getMousePosition().x - subregion.getWidth()/2, jlpMain.getMousePosition().y - subregion.getHeight()/2);
	}
	}
}
	public void playSound(final String sounds[]){
		final int randomIndex = (int)(Math.random()*sounds.length);
		Thread thread = new Thread(new Runnable(){

			@Override
			public void run() {
				Player.playSound(sounds[randomIndex]);
				
			}	});
		thread.start();
		
		}
	public void  playSound(final String sounds[], double probabilityPercentage){
		probabilityPercentage = ((probabilityPercentage <= 1)? probabilityPercentage: probabilityPercentage/100);// assuming the value is could only between the following magnitudes: 10^-2 - 10^2
		double randomNumber = Math.random();
		if(randomNumber <= probabilityPercentage){
			playSound(new String[]{this.getClass().getResource("Audio/Good Feedback/correct.wav").toString().replace("file:/", "").replaceAll("%20", " ")});
		}
		
		else{
			playSound(sounds);
		}
	}
	public void updateGame(boolean isCorrect){
		if(isCorrect){
			if(!isAlreadyAnsweredIncorrectly){
				correctSubregions.add(subregion.regionName);
				score++;
			}
			else{
				isAlreadyAnsweredIncorrectly = false;
			}
			
			questionCount++;
			jlScore.setText("Score: "+ score+"/"+questionCount+" of "+ subregions.length +" questions");
			
			//If the incorrectSubregion is displayed, then remove it
			jlpMain.remove(incorrectSubregion);
			jlpMain.repaint();
			
			subregion.setBounds(mapToBeDisplayed.getX() + subregion.correctLocation.x, mapToBeDisplayed.getY() + subregion.correctLocation.y, subregion.getWidth() , subregion.getHeight());
			
			jlpMain.setLayer(subregion, 1);
			subregion.removeMouseListener(releaseListener);
			subregion.removeMouseMotionListener(dragListener);
			setRandomSubregion();
			if(subregion != null){
			subregion.addMouseListener(releaseListener);
			subregion.addMouseMotionListener(dragListener);
			
			
			int subregionWidth = subregion.getPreferredSize().width;
			int subregionHeight = subregion.getPreferredSize().height;
			subregion.setBounds(jpDrag.getX() + (jpDrag.getWidth()/2) - (subregionWidth/2), jpDrag.getY()+ (jpDrag.getWidth()/2)-(subregionHeight/2),	subregionWidth, subregionHeight);
			jlpMain.add(subregion);
			jlpMain.setLayer(subregion, 2);
			}
		}
		else{
			
			int incorrectSubregionWidth = incorrectSubregion.getPreferredSize().width;
			int incorrectSubregionHeight = incorrectSubregion.getPreferredSize().height;
			incorrectSubregion.setBounds(mapToBeDisplayed.getX() + incorrectSubregion.correctLocation.x, mapToBeDisplayed.getY() + incorrectSubregion.correctLocation.y,	incorrectSubregionWidth, incorrectSubregionHeight);
			jlpMain.add(incorrectSubregion);
			jlpMain.setLayer(incorrectSubregion, 1);
		}
	}
	public void reset(){
		if(timer != null){
			timer.stop();
			score = 0; 
			questionCount = 0;
			minutes = 0;
			seconds = 0;
			jlScore.setText("Score: 0/0 of 0 questions");
		    jlTime.setText("Time: 0 minutes 0 seconds");
		    missedSubregions.clear();
		    correctSubregions.clear();
		    editModeIndex = 0;
		    
		}
	}
public static void main(String[] args){
	GameFrame gameFrame = new GameFrame();
	gameFrame.setTitle("Game Frame");
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	gameFrame.setSize(screenSize.width, screenSize.height - 40);
	gameFrame.setLocationRelativeTo(null);
	gameFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	gameFrame.setVisible(true);
}
}