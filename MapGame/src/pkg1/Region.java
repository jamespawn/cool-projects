package pkg1;
import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Scanner;

import javax.swing.*;

public class Region extends JLabel implements Cloneable {

	//Not sure if I need dimension but I'll keep it for now
protected Dimension dimension;//Initialized by the constructors


public  Point correctLocation;//subregions from input parameter 

//If the region is not a subregion then draggable is false
protected boolean draggable = false;

protected String regionType; //Initalized by constructors. examples of types: continent, country, state

protected String regionName; // From Input parameter
protected Region [] subregions; // Initalized by constructors
protected Region [] incorrectSubregions;
protected String subregionType;
protected Icon regionIcon; // Initalized by constructors
public String difficulty;
public File pointsFile;

public Region(String regionName, String difficulty){
	this.difficulty = difficulty;
this.regionName = regionName;
Class metaObject = this.getClass();
String regionURL = metaObject.getResource(regionName).toString().replaceAll("%20", " ");

File file = new File(regionURL.toString().replace("file:/", ""));

String[] files = file.list();

setRegionTypes(files);// sets regionType and subregionType

switch(difficulty){
case "Easy": this.regionIcon = new ImageIcon(file.toString() +"\\"+ regionName+" Borders.png");
break;
case "Hard": this.regionIcon = new ImageIcon(file.toString() +"\\"+ regionName+" Test.png");
}
setIcon(regionIcon);// sets the icon of this region

setHorizontalAlignment(SwingConstants.CENTER);
dimension = new Dimension(regionIcon.getIconWidth(), regionIcon.getIconHeight());
String subregionPath = regionURL.toString().replace("file:/", "")+"/"+ (subregionType.equals("State")? "States":"Countries");
String incorrectSubregionPath = regionURL.toString().replace("file:/", "")+"/Incorrect "+ (subregionType.equals("State")? "States":"Countries");
setSubregions(subregionPath, incorrectSubregionPath);


}
/*Finds the sub-region type then set the region type by reading 
 * the files under the region provided to the constructor.
 */
@SuppressWarnings("resource")
private void setSubregions(String subregionPath, String incorrectSubregionPath){
	File subregionFolder = new File(subregionPath);
	String[] files = subregionFolder.list();
	 subregions = new Region[files.length];
	
	
	File incorrectSubregionFolder = new File(incorrectSubregionPath);
	String[] incorrectSubregionFiles = incorrectSubregionFolder.list();
	 incorrectSubregions = new Region[incorrectSubregionFiles.length];
	
	
	Class metaObject = this.getClass();
	String pointsURL = this.getClass().getResource("ScoreBoard.class").toString().replace("ScoreBoard.class", "").replace("file:/", "").concat(regionName+"/Points.txt").replaceAll("%20", " ");
	System.out.println(pointsURL);
	
	Scanner input = null;
	try {
		pointsFile = new File(pointsURL.toString().replace("file:/", ""));
		
		if(pointsFile.exists()){
			input = new Scanner(pointsFile);
			if(input.hasNext()){
			for(int i = 0; i < subregions.length; i++){
				int x = input.nextInt();
				input.next();
				int y = input.nextInt();
				subregions[i] = new Region(files[i].replace(".png", ""), subregionType, new Point(x,y), subregionFolder.toString()+"/"+files[i] ); // don't forget to set tle location
				incorrectSubregions[i] = new Region(files[i], subregionType, new Point(x,y), incorrectSubregionFolder.toString()+"/"+incorrectSubregionFiles[i] );
		
		}
			}
			else{
				for(int i = 0; i < subregions.length; i++){
					subregions[i] = new Region(files[i].replace(".png", ""), subregionType, new Point(0,0), subregionFolder.toString()+"/"+files[i] ); // don't forget to set tle location
					incorrectSubregions[i] = new Region(files[i], subregionType, new Point(0,0), incorrectSubregionFolder.toString()+"/"+incorrectSubregionFiles[i] );
			
				}
			}
		}
		else{
			PrintWriter output = new PrintWriter(pointsURL);
			output.print("");
			output.close();
			for(int i = 0; i < subregions.length; i++){
				subregions[i] = new Region(files[i].replace(".png", ""), subregionType, new Point(0,0), subregionFolder.toString()+"/"+files[i] ); // don't forget to set tle location
				incorrectSubregions[i] = new Region(files[i], subregionType, new Point(0,0), incorrectSubregionFolder.toString()+"/"+incorrectSubregionFiles[i] );
		
			}
		}
		
	} catch (FileNotFoundException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}

	
		
	
	
	
	


}
private void setRegionTypes(String[] files){
	for(String e: files){
if(e.toLowerCase().equals("countries")){
			subregionType = "Country";
			regionType = "Continent";
			break;
}
else if(e.toLowerCase().equals("states")){
			subregionType = "State";
			regionType = "Country";
			break;
			
		}
	}
}
private Region (String subregionName, String subregionType, Point subregionCorrectLocation, String subregionIconLocation){
	this.regionName = subregionName;
	this.regionType = subregionType;
	this.correctLocation = subregionCorrectLocation;
	this.regionIcon = new ImageIcon(subregionIconLocation);
	setIcon(regionIcon);
	setHorizontalAlignment(SwingConstants.CENTER);
	this.dimension = new Dimension(regionIcon.getIconWidth(), regionIcon.getIconHeight());
	this.draggable = true;
	
}

//set when current and correct are close enough
public void setDraggable(boolean draggable){
	this.draggable = draggable;
}
//Will be used to set label information
public String getRegionType(){
	return regionType;
}

public String getSubregionType(){
	return subregionType;
}
//returns this region's name
public String getRegionName(){
	return regionName;
}
//Will be used to generate a random subregion to drag onto region
public Region[] getSubregions(){
	return subregions;
}
public Region[] getIncorrectSubregions(){
	return incorrectSubregions;
}
public int getNumberOfSubregions(){
	return subregions.length;
}
public String getSubregionPlural(){
if(subregionType.equalsIgnoreCase("State")){
	return "States";
}
else if (subregionType.equalsIgnoreCase("Country")){
return "Countries";
}
return null;
}
public static void main(String [] args){
	Region r = new Region("United States", "Hard");
	JFrame frame = new JFrame();
	frame.add(r);
	frame.pack();
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	frame.setVisible(true);
}

}
