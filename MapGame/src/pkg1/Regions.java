package pkg1;

import java.io.File;
import java.util.ArrayList;

public final class Regions {
	ArrayList<String> regionNames = new ArrayList<>();
	public Regions(){
		
	}
	public final String[] getRegions(){
		String path = this.getClass().getResource("Regions.class").toString().replace("Regions.class", "").replace("file:/", "");
		File regionsLocation = new File(path);
		String[] files = regionsLocation.list();
		for(String e: files){
			regionsLocation = new File(path+"/" + e);
	
			if(regionsLocation.isDirectory() && (!regionsLocation.equals(new File(path+"Audio")))){
				regionNames.add(e);
			}
		}
		return regionNames.toArray(new String[]{});
	}

}
