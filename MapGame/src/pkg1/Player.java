package pkg1;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.Scanner;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;


/**
 * An example of loading and playing a sound using a Clip. This complete class
 * isn't in the book ;)
 */
public class Player {
	
  static void playSound(String sound){
	 try{
		  Clip clip = AudioSystem.getClip();
	
			  clip.open(AudioSystem.getAudioInputStream(new File(sound))); 
		
			  clip.start();
			 Thread.sleep(clip.getMicrosecondLength()/1000);
	 }
	 catch(Exception e){
		 
	 }
	 
  }
}        