package pkg1;
import java.awt.*;
import java.io.*;
import java.net.URL;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;

public class ScoreBoard extends JPanel {

	JPanel jpMain = new JPanel(new BorderLayout());
	DefaultListModel dlmTime = new DefaultListModel();
	DefaultListModel dlmScore = new DefaultListModel();
	JList jlTime = new JList(dlmTime);
	JList jlScore = new JList(dlmScore);
	JScrollPane jspTime = new JScrollPane(jlTime);
	JScrollPane jspScore = new JScrollPane(jlScore);
	ArrayList<String> regionNames = new ArrayList<>();
	private static ScoreBoard scoreBoard;
	boolean writeToFile;
	public static ScoreBoard getInstance(String difficulty) throws FileNotFoundException{
		scoreBoard = new ScoreBoard(difficulty);
	
		return scoreBoard;
	}
	public static ScoreBoard getInstance(String playerName, Region region, int score, int minutes, int seconds) throws FileNotFoundException{
		scoreBoard = new ScoreBoard(playerName, region, score, minutes, seconds);
		return scoreBoard;
	}
	private ScoreBoard(String playerName, Region region, int playerScore, int playerMinutes, int playerSeconds ) throws FileNotFoundException{
		setup();
		String path = this.getClass().getResource("ScoreBoard.class").toString().replace("ScoreBoard.class", "").replace("file:/", "").toString().replaceAll("%20", " ") + region.difficulty+" Scores.txt";
		File scoreFile = new File(path);
		
		Scanner input = new Scanner(scoreFile);
		int index = 0;
		
		
		//******* if the score file is empty then insert the results.
	
		if(!input.hasNext()){
			PrintWriter output = new PrintWriter(scoreFile);
			output.println(region.regionName);
			output.println(playerName + " " + playerScore + " " + region.getNumberOfSubregions());
			output.println(playerName + " " + playerMinutes +  " " + playerSeconds);
			output.close();
			dlmScore.addElement(region.regionName + ": " + playerName+": " + playerScore + "/" + region.getNumberOfSubregions());
	
			dlmTime.addElement(region.regionName +": " + playerName + ": " + playerMinutes+ " "+((playerMinutes == 1)? "minute":"minutes")+ " "+ playerSeconds + " " + ((playerSeconds == 1)? "second":"seconds"));
		output.close();
		}
		else{
	while(input.hasNext()){
	
		String currentLine;
		String regionToDisplay = input.nextLine();
		regionNames.add(regionToDisplay);
		String currentTime = "";
		String currentScore = "";
		//extracting score information
			currentLine = input.nextLine();
			Scanner scoreScanner = new Scanner(currentLine);
			String scoreName = scoreScanner.next();
			int scoreToDisplay = scoreScanner.nextInt();
			int totalQuestions = scoreScanner.nextInt();
			if(regionToDisplay.equals(region.regionName) && scoreToDisplay < playerScore){
				scoreName = playerName;
				scoreToDisplay = playerScore;
				writeToFile = true;
			}
				currentScore +=regionToDisplay + ": " + scoreName+": " + scoreToDisplay + "/" + totalQuestions;
				dlmScore.addElement(currentScore);
		
				
				//extracting time record
				currentLine = input.nextLine();
				Scanner timeScanner = new Scanner(currentLine);
				String timeName = timeScanner.next();
				int secondsToDisplay;
			
				
				if(currentLine.matches("(\\w+)(\\s+)(\\d+)(\\s+)((\\d)|([0-5](\\d)))")){//Format: Name d... d || dd
					int minutesToDisplay = timeScanner.nextInt();
					 secondsToDisplay = timeScanner.nextInt();
						if(regionToDisplay.equals(region.regionName) && (minutesToDisplay > playerMinutes || (minutesToDisplay  == playerMinutes && secondsToDisplay > playerSeconds))){
							timeName = playerName;
							minutesToDisplay = playerMinutes;
							secondsToDisplay = playerSeconds;
							writeToFile = true;
						}
					currentTime += regionToDisplay +": " + timeName + ": " + minutesToDisplay+ " "+((minutesToDisplay == 1)? "minute":"minutes")+ " "+ secondsToDisplay + " " + ((secondsToDisplay == 1)? "second":"seconds");
					dlmTime.addElement(currentTime);
				}
				
			
				index++;
		timeScanner.close();
		}
	if(!isInRegionNames(region.regionName)){
		dlmScore.addElement(region.regionName + ": " + playerName+": " + playerScore + "/" + region.subregions.length);
		dlmTime.addElement(region.regionName +": " + playerName + ": " + playerMinutes+ " "+((playerMinutes == 1)? "minute":"minutes")+ " "+ playerSeconds + " " + ((playerSeconds == 1)? "second":"seconds"));
		writeToFile = true;
	}
		}
	input.close();
	if(writeToFile){
		sortRecords();
		PrintWriter output = new PrintWriter(scoreFile);
		for(int i = 0; i < dlmScore.size(); i++){
			String scoreLine = (String)dlmScore.get(i);
			Scanner lineReader = new Scanner(scoreLine);
			lineReader.useDelimiter(": ");
			
			String regionString = lineReader.next();
			
			String nameString = lineReader.next();
			String temp = lineReader.next();
			String scoreString = temp.substring(0, temp.indexOf('/'));
			String totalQuestionsString = temp.substring(temp.indexOf('/') + 1, temp.length());
			output.println(regionString);
			output.println(nameString + " " + scoreString +" " + totalQuestionsString);
			lineReader.close();
			
			String timeLine = (String) dlmTime.get(i);
			lineReader = new Scanner(timeLine);
			lineReader.useDelimiter(": ");
			lineReader.next();
			lineReader.next();
			temp = lineReader.next();
			lineReader.close();
			lineReader = new Scanner(temp);
			int minutes = lineReader.nextInt();
			lineReader.next();
			int seconds = lineReader.nextInt();
			lineReader.next();
			lineReader.close();
			output.print(nameString + " " + minutes + " " + seconds  + "\n");
		}
		writeToFile = false;
		output.close();
	}
	jpMain.add(jspScore, BorderLayout.WEST);
	jpMain.add(jspTime, BorderLayout.EAST);
		
	
		
	}
	private ScoreBoard(String difficulty) throws FileNotFoundException  {
		setup();
	String path = this.getClass().getResource("ScoreBoard.class").toString().replace("ScoreBoard.class", "").replace("file:/", "").toString().replaceAll("%20", " ") + difficulty+" Scores.txt";
	File scoreFile = new File(path);
	Scanner input = new Scanner(scoreFile);

	
		int index = 0;
		//******* if the score file is empty then is should notify the user.
		
	while(input.hasNext()){
		String currentLine;
		String region = input.nextLine();
		String currentTime = "";
		String currentScore = "";
		
		//extracting score information
			currentLine = input.nextLine();
			Scanner scoreScanner = new Scanner(currentLine);
			String name1 = scoreScanner.next();
			int score = scoreScanner.nextInt();
			int totalQuestions = scoreScanner.nextInt();
	
				currentScore +=region + ": " + name1+": " + score + "/" +totalQuestions ;
				dlmScore.addElement(currentScore);
		
				
				//extracting time record
				currentLine = input.nextLine();
				Scanner timeScanner = new Scanner(currentLine);
				String name2 = timeScanner.next();
				int seconds;
				
				if(currentLine.matches("(\\w+)(\\s+)(\\d+)(\\s+)((\\d)|([0-5](\\d)))")){//Format: Name d... d || dd
					int minutes = timeScanner.nextInt();
					 seconds = timeScanner.nextInt();
					currentTime += region +": " + name2 + ": " + minutes+ " "+((minutes == 1)? "minute":"minutes")+ " "+ seconds + " " + ((seconds == 1)? "second":"seconds");
					dlmTime.addElement(currentTime);
				}
				
			
				index++;
		timeScanner.close();
		}

	input.close();
	jpMain.add(jspScore, BorderLayout.WEST);
	jpMain.add(jspTime, BorderLayout.EAST);
	}
	public void setup(){
		jpMain.setBorder(new TitledBorder("Highest players ranked by: "));
		jspTime.setBorder(new TitledBorder("Time"));
		jlTime.setBackground(jpMain.getBackground());
		jspScore.setBorder(new TitledBorder("Score"));
		jlScore.setBackground(jpMain.getBackground());
		
		setLayout(new BorderLayout()); //for packing the frame in MainMenu	
	add(jpMain, BorderLayout.NORTH);
	}

	public static void main(String [] args) throws FileNotFoundException{
		ScoreBoard.getInstance("Mahmoud", new Region("United States", "Easy"), 48, 1, 12); //This is what may throw an exception
	}
	public void sortRecords(){
		for(int i = 0; i < dlmTime.size(); i++){
			String temp = (String) dlmTime.get(i);
		String currentMinimumRegion = temp.substring(0, temp.indexOf(':'));
		for(int j = i+1; j < dlmTime.size(); j++){
		String regionToBeComparedWith = (String)dlmTime.get(j);
		if(currentMinimumRegion.compareTo(regionToBeComparedWith) > 0){
			currentMinimumRegion = regionToBeComparedWith;
			Object dlmTimeElement = dlmTime.get(j);
			dlmTime.set(j, dlmTime.get(i));
			dlmTime.set(i, dlmTimeElement);
			
			
			Object dlmScoreElement = dlmScore.get(j);
			dlmScore.set(j, dlmScore.get(i));
			dlmScore.set(i, dlmScoreElement);
		}
		}
		}
	}
	public boolean isInRegionNames(String regionName){
		for(String e: regionNames){
			if(regionName.equals(e)){
				return true;
			}
		}
		return false;
	}
}