package pkg1;

import java.awt.*;
import java.io.FileNotFoundException;

import javax.swing.*;
import javax.swing.border.TitledBorder;

public class Results extends JFrame{

	int score;
	int minutes;
	int seconds;
	Region region;
	Results(String playerName, Region region, int score, int minutes, int seconds, String[] correctSubregions, String[] missedSubregions){
		this.score = score;
		this.minutes = minutes;
		this.seconds = seconds;
		this.region = region;
		setLayout(new BorderLayout());
		//Displays two scroll panes each with a JList. One JList displays the subregions scored correct while the other displays the subregions missed
		JScrollPane jspMissed = new JScrollPane(new JList(missedSubregions));
		jspMissed.setBorder(new TitledBorder(region.getSubregionPlural() + " Missed"));
		JScrollPane jspCorrect = new JScrollPane(new JList(correctSubregions));
		jspCorrect.setBorder(new TitledBorder(region.getSubregionPlural() + " Answered Correctly"));
		JPanel jpComboBoxPanel = new JPanel(new GridLayout(2,0));
		jpComboBoxPanel.add(jspMissed);
		jpComboBoxPanel.add(jspCorrect);
		
		
		
		
		JPanel jpEast = new JPanel(new BorderLayout(5, 30));
		JLabel resultsSummary = new JLabel(getResult());
		resultsSummary.setBorder(new TitledBorder("Result:"));
		jpEast.add(resultsSummary, BorderLayout.NORTH);

		try {
			jpEast.add(ScoreBoard.getInstance(playerName, region, score, minutes, seconds)); //This is what may throw an exception
			
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			jpComboBoxPanel.setPreferredSize(new Dimension(screenSize.width/5, jpComboBoxPanel.getPreferredSize().height));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		add(jpComboBoxPanel, BorderLayout.WEST);
		add(jpEast, BorderLayout.EAST);
	}
	public static void main(String[] args){
		JFrame frame = new Results("Mahmoud", new Region("United States", "Easy"), 50, 1, 10, new String[]{"Alabama"}, new String[]{"Alaska"});
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
	public String getResult(){
		String result = "<html>Score: " + score + "/" + region.getNumberOfSubregions() + " Time: " + minutes + ((minutes == 1)? " mintue ":" minutes " )  + seconds + ((seconds == 1)? " second" : " seconds" +"<br>Feedback: ");
		int percentage = ((100 * score)/ region.getNumberOfSubregions());
		String aVersesAn = "";
		if(percentage == 8 || percentage == 11 || percentage == 18 || percentage == 88){
			aVersesAn = "an";
		}
		else{
			aVersesAn = "a";
		}
		result += " You got " + aVersesAn + " " + percentage + "%! ";
		if( percentage == 100){
			result += "That's a perfect score! Great job!";
		}
		else if(percentage <= 99 && percentage >= 90){
			result += "You did well!";
		}
		else if(percentage <= 89 && percentage >= 75){
			result += "You did O.K!";
		}
		else if(percentage <= 74 && percentage >= 50){
			result += "You can do better! You need more practice.";
		}
		else{
			result +="You failed! You need more practice. Don't give up!";
		}
		result += "</html>";
		return result;
	}
}